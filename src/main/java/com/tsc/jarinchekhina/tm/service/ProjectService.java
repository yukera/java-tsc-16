package com.tsc.jarinchekhina.tm.service;

import com.tsc.jarinchekhina.tm.api.repository.IProjectRepository;
import com.tsc.jarinchekhina.tm.api.service.IProjectService;
import com.tsc.jarinchekhina.tm.enumerated.Status;
import com.tsc.jarinchekhina.tm.exception.AbstractException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyDescriptionException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyIdException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyNameException;
import com.tsc.jarinchekhina.tm.exception.entity.ProjectNotFoundException;
import com.tsc.jarinchekhina.tm.exception.system.IndexIncorrectException;
import com.tsc.jarinchekhina.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public Project create(final String name) throws EmptyNameException {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = new Project();
        project.setName(name);
        projectRepository.add(project);
        return project;
    }

    @Override
    public Project create(final String name, final String description) throws AbstractException {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(project);
        return project;
    }

    @Override
    public void add(final Project project) {
        if (project == null) return;
        projectRepository.add(project);
    }

    @Override
    public void remove(final Project project) {
        if (project == null) return;
        projectRepository.remove(project);
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public List<Project> findAll(final Comparator<Project> comparator) {
        if (comparator == null) return null;
        return projectRepository.findAll(comparator);
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

    @Override
    public Project findOneById(final String id) throws EmptyIdException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.findOneById(id);
    }

    @Override
    public Project findOneByIndex(final Integer index) throws IndexIncorrectException {
        if (index == null || index < 0) throw new IndexIncorrectException();
        return projectRepository.findOneByIndex(index);
    }

    @Override
    public Project findOneByName(final String name) throws EmptyNameException {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.findOneByName(name);
    }

    @Override
    public Project removeOneById(final String id) throws EmptyIdException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.removeOneById(id);
    }

    @Override
    public Project removeOneByIndex(final Integer index) throws IndexIncorrectException {
        if (index == null || index < 0) throw new IndexIncorrectException();
        return projectRepository.removeOneByIndex(index);
    }

    @Override
    public Project removeOneByName(final String name) throws EmptyNameException {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.removeOneByName(name);
    }

    @Override
    public Project updateProjectById(final String id, final String name, final String description) throws AbstractException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateProjectByIndex(final Integer index, final String name, final String description) throws AbstractException {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project startProjectById(final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Project project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project startProjectByIndex(final Integer index) throws AbstractException {
        if (index == null || index < 0) throw new IndexIncorrectException();
        final Project project = findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project startProjectByName(final String name) throws AbstractException {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = findOneByName(name);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project finishProjectById(final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Project project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.COMPLETED);
        return project;
    }

    @Override
    public Project finishProjectByIndex(final Integer index) throws AbstractException {
        if (index == null || index < 0) throw new IndexIncorrectException();
        final Project project = findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.COMPLETED);
        return project;
    }

    @Override
    public Project finishProjectByName(final String name) throws AbstractException {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = findOneByName(name);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.COMPLETED);
        return project;
    }

    @Override
    public Project changeProjectStatusById(final String id, final Status status) throws AbstractException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Project project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeProjectStatusByIndex(final Integer index, final Status status) throws AbstractException {
        if (index == null || index < 0) throw new IndexIncorrectException();
        final Project project = findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeProjectStatusByName(final String name, final Status status) throws AbstractException {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = findOneByName(name);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

}
