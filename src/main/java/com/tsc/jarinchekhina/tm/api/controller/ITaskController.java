package com.tsc.jarinchekhina.tm.api.controller;

import com.tsc.jarinchekhina.tm.exception.AbstractException;

public interface ITaskController {

    void showTasks();

    void clearTasks();

    void createTask() throws AbstractException;

    void showTaskById() throws AbstractException;

    void showTaskByIndex() throws AbstractException;

    void showTaskByName() throws AbstractException;

    void updateTaskById() throws AbstractException;

    void updateTaskByIndex() throws AbstractException;

    void removeTaskById() throws AbstractException;

    void removeTaskByIndex() throws AbstractException;

    void removeTaskByName() throws AbstractException;

    void startTaskById() throws AbstractException;

    void startTaskByIndex() throws AbstractException;

    void startTaskByName() throws AbstractException;

    void finishTaskById() throws AbstractException;

    void finishTaskByIndex() throws AbstractException;

    void finishTaskByName() throws AbstractException;

    void changeTaskStatusById() throws AbstractException;

    void changeTaskStatusByIndex() throws AbstractException;

    void changeTaskStatusByName() throws AbstractException;

    void findAllTaskByProjectId() throws AbstractException;

    void bindTaskByProjectId() throws AbstractException;

    void unbindTaskByProjectId() throws AbstractException;

}
